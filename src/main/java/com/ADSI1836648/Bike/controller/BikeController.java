package com.ADSI1836648.Bike.controller;

import com.ADSI1836648.Bike.model.Bike;
import com.ADSI1836648.Bike.service.BikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class BikeController {
    //guardar y crear una bicicleta
    @Autowired
    BikeService bikeService;

    @PostMapping("/bike")
    public Bike create(@RequestBody Bike bike) {
        return bikeService.save(bike);
    }

    // Get - obtener - read
    @GetMapping("/bike")
    public Iterable<Bike> read() {
        return bikeService.findAll();
    }

    // Put - actualizar - update
    @PutMapping("/bike")
    public Bike update(@RequestBody Bike bike) {
        return bikeService.save(bike);
    }

    //Delete - borrar - delete
    @DeleteMapping("/bike/{id}")
    public void delete(@PathVariable Integer id) {
        bikeService.deleteById(id);
    }

    @GetMapping("/bike/{id}")
    public Optional<Bike> getById(@PathVariable Integer id){
        return bikeService.findById(id);
    }
}


