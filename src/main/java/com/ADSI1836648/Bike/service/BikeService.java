package com.ADSI1836648.Bike.service;

import com.ADSI1836648.Bike.model.Bike;
import org.springframework.data.repository.CrudRepository;

public interface BikeService extends CrudRepository<Bike, Integer> {
}
