package com.ADSI1836648.Bike.model;

import javax.persistence.*;
import java.time.Instant;

@Entity
public class Sale {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private int id;


    private Instant dateSale;

    @ManyToOne
    private Bike bike;

    @ManyToOne
    private Client client;

    public Sale() {
    }

    public Sale(Instant dateSale, Bike bike, Client client) {
        this.dateSale = dateSale;
        this.bike = bike;
        this.client = client;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Instant getDateSale() {
        return dateSale;
    }

    public void setDateSale(Instant dateSale) {
        this.dateSale = dateSale;
    }

    public Bike getBike() {
        return bike;
    }

    public void setBike(Bike bike) {
        this.bike = bike;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
