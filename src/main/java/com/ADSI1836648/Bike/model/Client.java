package com.ADSI1836648.Bike.model;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.*;


@Entity
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private int id;

    @JsonProperty("client-name")
    private String name;

    @JsonProperty("client-email")
    private  String email;
    @JsonProperty("phone-number")
    private String phoneNumber;
    @JsonProperty("document-number")
    private String documentNumber;

    public Client (){

    }
    public Client(int id, String name, String email, String phoneNumber, String documentNumber ){
        this.id = id;
        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.documentNumber = documentNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
