FROM openjdk:8-jdk-alpine as build
WORKDIR /api
VOLUME /tmp
ARG JAR_FILE
COPY ./target/Bike-0.0.1-SNAPSHOT.jar .
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","Bike-0.0.1-SNAPSHOT.jar"]

